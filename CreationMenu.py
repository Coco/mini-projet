# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 22:42:57 2017

@author: Vincent
"""

import os

def effaceEcran():
    """Efface l’écran de la console"""
    os.system('cls' if os.name == 'nt' else 'clear')
  
def affichageMenu():
    """Affiche les choix du menu"""
    print("1 : Modifier la valeur de a")
    print("2 : Modifier la valeur de b")
    print("3 : Afficher la valeur de a+b")
    print("4 : Afficher la valeur de a-b")
    print("5 : Afficher la valeur de a*b")
    print("6 : Afficher la valeur de a/b")
    print("q : quitter le programme")
    
def affichageVariables(a,b):
    """Affiche les valeurs des deux variables a et b"""
    print("a=",a)
    print("b=",b)
    
def demandeValeur(nomVariable,valeurInitiale):
    """Demande la nouvelle valeur pour une variable :
        - nommée nomVariable 
        - ayant pour valeur initiale ValeurInitiale
    Valeur de retour :
        - la nouvelle valeur si un nombre a été entré
        - la valeur initiale sinon avec un message d’erreur"""
    try:
        x=float(input("Nouvelle valeur de "+nomVariable+" :"))
        effaceEcran()
    except:
        x=valeurInitiale
        effaceEcran()
        print(nomVariable,"doit être un nombre !")
    return x
    
def affichageSomme(a,b):
    """Efface l’écran, affiche la valeur des deux variables
    et affiche la somme"""
    effaceEcran()
    affichageVariables(a,b)
    print("a+b=",a+b,end="\n\n")
    
def affichageDifference(a,b):
    """Efface l’écran, affiche la valeur des deux variables
    et affiche la différence"""
    effaceEcran()
    affichageVariables(a,b)
    print("a-b=",a-b,end="\n\n")
    
def affichageProduit(a,b):
    """Efface l’écran, affiche la valeur des deux variables
    et affiche le produit"""
    effaceEcran()
    affichageVariables(a,b)
    print("a*b=",a*b,end="\n\n")
    
def affichageQuotient(a,b):
    """Efface l’écran, affiche la valeur des deux variables
    et affiche le quotient 
    avec un message d’erreur en cas de division par 0"""
    effaceEcran()
    affichageVariables(a,b)
    if b!=0:
        print("a/b=",a/b,end="\n\n")
    else:
        print("Division par 0 impossible !",end="\n\n")


a=0		#initialisation des variable
b=0
affichageVariables(a,b)
choix="0"	#initialisation du choix
while choix!="q":
    affichageMenu()
    choix=input("Choisir une action : ")
    if choix=="1":
        a=demandeValeur("a",a)
        affichageVariables(a,b)
    elif choix=="2":
        b=demandeValeur("b",b)
        affichageVariables(a,b)
    elif choix=="3":
        affichageSomme(a,b)
    elif choix=="4":
        affichageDifference(a,b)
    elif choix=="5":
        affichageProduit(a,b)
    elif choix=="6":
        affichageQuotient(a,b)
    elif choix.upper()=="Q":
        effaceEcran()
        print("Au revoir")
    else:		#si aucune choix défini n’a été entré, on réaffiche juste les variables
        effaceEcran()
        affichageVariables(a,b)
    
        