# Mini-projet : Traitement d'image
Les élèves devront produire un programme capable de réaliser les traitements
suivants sur des images fournies en entrée du programme :
 * négatif
 * rotation de couleur : RGB -> BRG, RGB -> BGR, RGB -> GBR
 * rotation des pixels : -90°, +90°, 180°
 * symmétrie axiale : verticale, horizontale

## Pré-requis
 * code source de base d'un programme fonctionnel permettant l'ouverture d'un
 fichier image, la modification de son contenu et l'affichage des images source
 et traitée
 * Conteneurs Python3
 * TP multimedia niveau 1 et 1.5 (les compléments)
 * DM n°1

## Apport de connaissances
Les apports notionnels suivants devront être couverts pendant le déroulement du
mini-projet :
 * algorithmie spécifique aux traitements à faire
 * fonctions et sous-programmes en Python3
 * gestion des images : utilisation de la bibliothèque PIL (voir https://github.com/python-pillow/Pillow/tree/2.9.x/docs)
